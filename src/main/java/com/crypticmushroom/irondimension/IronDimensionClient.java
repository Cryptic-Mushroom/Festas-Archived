package com.crypticmushroom.irondimension;

import com.crypticmushroom.irondimension.entities.entity.FerrobearEntity;
import com.crypticmushroom.irondimension.entities.entity.IronBisonEntity;
import com.crypticmushroom.irondimension.entities.entity.SilverHornEntity;
import com.crypticmushroom.irondimension.entities.renderer.FerrobearRenderer;
import com.crypticmushroom.irondimension.entities.renderer.IronBisonRenderer;
import com.crypticmushroom.irondimension.entities.renderer.SilverHornRenderer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.render.EntityRendererRegistry;

/**
 * IronDimensionClient
 */
public class IronDimensionClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.INSTANCE.register(IronBisonEntity.class,
                (entityRenderDispatcher, context) -> new IronBisonRenderer(entityRenderDispatcher));
        EntityRendererRegistry.INSTANCE.register(SilverHornEntity.class,
                (entityRenderDispatcher, context) -> new SilverHornRenderer(entityRenderDispatcher));
        EntityRendererRegistry.INSTANCE.register(FerrobearEntity.class,
                (entityRenderDispatcher, context) -> new FerrobearRenderer(entityRenderDispatcher));
    }
}