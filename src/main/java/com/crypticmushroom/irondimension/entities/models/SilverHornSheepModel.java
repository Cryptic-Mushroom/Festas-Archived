package com.crypticmushroom.irondimension.entities.models;

import com.crypticmushroom.irondimension.entities.entity.SilverHornEntity;
import net.minecraft.client.model.Cuboid;
import net.minecraft.client.render.entity.model.QuadrupedEntityModel;

public class SilverHornSheepModel<T extends SilverHornEntity> extends QuadrupedEntityModel<T> {
    private float field_3552;

    public SilverHornSheepModel() {
        super(12, 0.0F);
        this.head = new Cuboid(this, 0, 0);
        this.head.addBox(-3.0F, -4.0F, -6.0F, 6, 6, 8, 0.0F);
        this.head.setRotationPoint(0.0F, 6.0F, -8.0F);
        this.body = new Cuboid(this, 28, 8);
        this.body.addBox(-4.0F, -10.0F, -7.0F, 8, 16, 6, 0.0F);
        this.body.setRotationPoint(0.0F, 5.0F, 2.0F);
    }

    public void method_17120(T sheepEntity_1, float float_1, float float_2, float float_3) {
        super.animateModel(sheepEntity_1, float_1, float_2, float_3);
        this.head.rotationPointY = 6.0F + sheepEntity_1.method_6628(float_3) * 9.0F;
        this.field_3552 = sheepEntity_1.method_6641(float_3);
    }

    public void method_17121(T sheepEntity_1, float float_1, float float_2, float float_3, float float_4, float float_5, float float_6) {
        super.setAngles(sheepEntity_1, float_1, float_2, float_3, float_4, float_5, float_6);
        this.head.pitch = this.field_3552;
    }
}
