package com.crypticmushroom.irondimension.entities.entity;

import com.crypticmushroom.irondimension.registry.EntitiesID;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.FoxEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IWorld;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.World;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;

public class FerrobearEntity extends IronDimensionEntity {
    private static final TrackedData<Boolean> WARNING;
    private float lastWarningAnimationProgress;
    private float warningAnimationProgress;
    private int warningSoundCooldown;

    public FerrobearEntity(EntityType<? extends FerrobearEntity> entityType_1, World world_1) {
        super(entityType_1, world_1);
    }

    public PassiveEntity createChild(PassiveEntity passiveEntity_1) {
        return (PassiveEntity) EntitiesID.FERROBEAR.create(this.world);
    }

    public boolean isBreedingItem(ItemStack itemStack_1) {
        return false;
    }

    protected void initGoals() {
        super.initGoals();
        this.goalSelector.add(0, new SwimGoal(this));
        this.goalSelector.add(1, new FerrobearEntity.AttackGoal());
        this.goalSelector.add(1, new FerrobearEntity.PolarBearEscapeDangerGoal());
        this.goalSelector.add(4, new FollowParentGoal(this, 1.25D));
        this.goalSelector.add(5, new WanderAroundGoal(this, 1.0D));
        this.goalSelector.add(6, new LookAtEntityGoal(this, PlayerEntity.class, 6.0F));
        this.goalSelector.add(7, new LookAroundGoal(this));
        this.targetSelector.add(1, new FerrobearEntity.PolarBearRevengeGoal());
        this.targetSelector.add(2, new FerrobearEntity.FollowPlayersGoal());
    }

    protected void initAttributes() {
        super.initAttributes();
        this.getAttributeInstance(EntityAttributes.MAX_HEALTH).setBaseValue(30.0D);
        this.getAttributeInstance(EntityAttributes.FOLLOW_RANGE).setBaseValue(20.0D);
        this.getAttributeInstance(EntityAttributes.MOVEMENT_SPEED).setBaseValue(0.25D);
        this.getAttributeContainer().register(EntityAttributes.ATTACK_DAMAGE);
        this.getAttributeInstance(EntityAttributes.ATTACK_DAMAGE).setBaseValue(6.0D);
    }

    protected SoundEvent getAmbientSound() {
        return this.isBaby() ? SoundEvents.ENTITY_POLAR_BEAR_AMBIENT_BABY : SoundEvents.ENTITY_POLAR_BEAR_AMBIENT;
    }

    protected SoundEvent getHurtSound(DamageSource damageSource_1) {
        return SoundEvents.ENTITY_POLAR_BEAR_HURT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_POLAR_BEAR_DEATH;
    }

    protected void playStepSound(BlockPos blockPos_1, BlockState blockState_1) {
        this.playSound(SoundEvents.ENTITY_POLAR_BEAR_STEP, 0.15F, 1.0F);
    }

    protected void playWarningSound() {
        if (this.warningSoundCooldown <= 0) {
            this.playSound(SoundEvents.ENTITY_POLAR_BEAR_WARNING, 1.0F, this.getSoundPitch());
            this.warningSoundCooldown = 40;
        }

    }

    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(WARNING, false);
    }

    public void tick() {
        super.tick();
        if (this.world.isClient) {
            if (this.warningAnimationProgress != this.lastWarningAnimationProgress) {
                this.calculateDimensions();
            }

            this.lastWarningAnimationProgress = this.warningAnimationProgress;
            if (this.isWarning()) {
                this.warningAnimationProgress = MathHelper.clamp(this.warningAnimationProgress + 1.0F, 0.0F, 6.0F);
            } else {
                this.warningAnimationProgress = MathHelper.clamp(this.warningAnimationProgress - 1.0F, 0.0F, 6.0F);
            }
        }

        if (this.warningSoundCooldown > 0) {
            --this.warningSoundCooldown;
        }

    }

    public EntityDimensions getDimensions(EntityPose entityPose_1) {
        if (this.warningAnimationProgress > 0.0F) {
            float float_1 = this.warningAnimationProgress / 6.0F;
            float float_2 = 1.0F + float_1;
            return super.getDimensions(entityPose_1).scaled(1.0F, float_2);
        } else {
            return super.getDimensions(entityPose_1);
        }
    }

    public boolean tryAttack(Entity entity_1) {
        boolean boolean_1 = entity_1.damage(DamageSource.mob(this), (float)((int)this.getAttributeInstance(EntityAttributes.ATTACK_DAMAGE).getValue()));
        if (boolean_1) {
            this.dealDamage(this, entity_1);
        }

        return boolean_1;
    }

    public boolean isWarning() {
        return (Boolean)this.dataTracker.get(WARNING);
    }

    public void setWarning(boolean boolean_1) {
        this.dataTracker.set(WARNING, boolean_1);
    }

    @Environment(EnvType.CLIENT)
    public float getWarningAnimationProgress(float float_1) {
        return MathHelper.lerp(float_1, this.lastWarningAnimationProgress, this.warningAnimationProgress) / 6.0F;
    }

    protected float getBaseMovementSpeedMultiplier() {
        return 0.98F;
    }

    public EntityData initialize(IWorld iWorld_1, LocalDifficulty localDifficulty_1, SpawnType spawnType_1, EntityData entityData_1,  CompoundTag compoundTag_1) {
        if (entityData_1 instanceof FerrobearEntity.PolarBearEntityData) {
            this.setBreedingAge(-24000);
        } else {
            entityData_1 = new FerrobearEntity.PolarBearEntityData();
        }

        return (EntityData)entityData_1;
    }

    static {
        WARNING = DataTracker.registerData(FerrobearEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
    }

    class PolarBearEscapeDangerGoal extends EscapeDangerGoal {
        public PolarBearEscapeDangerGoal() {
            super(FerrobearEntity.this, 2.0D);
        }

        public boolean canStart() {
            return !FerrobearEntity.this.isBaby() && !FerrobearEntity.this.isOnFire() ? false : super.canStart();
        }
    }

    class AttackGoal extends MeleeAttackGoal {
        public AttackGoal() {
            super(FerrobearEntity.this, 1.25D, true);
        }

        protected void attack(LivingEntity livingEntity_1, double double_1) {
            double double_2 = this.getSquaredMaxAttackDistance(livingEntity_1);
            if (double_1 <= double_2 && this.ticksUntilAttack <= 0) {
                this.ticksUntilAttack = 20;
                this.mob.tryAttack(livingEntity_1);
                FerrobearEntity.this.setWarning(false);
            } else if (double_1 <= double_2 * 2.0D) {
                if (this.ticksUntilAttack <= 0) {
                    FerrobearEntity.this.setWarning(false);
                    this.ticksUntilAttack = 20;
                }

                if (this.ticksUntilAttack <= 10) {
                    FerrobearEntity.this.setWarning(true);
                    FerrobearEntity.this.playWarningSound();
                }
            } else {
                this.ticksUntilAttack = 20;
                FerrobearEntity.this.setWarning(false);
            }

        }

        public void stop() {
            FerrobearEntity.this.setWarning(false);
            super.stop();
        }

        protected double getSquaredMaxAttackDistance(LivingEntity livingEntity_1) {
            return (double)(4.0F + livingEntity_1.getWidth());
        }
    }

    class FollowPlayersGoal extends FollowTargetGoal<PlayerEntity> {
        public FollowPlayersGoal() {
            super(FerrobearEntity.this, PlayerEntity.class, 20, true, true, (Predicate)null);
        }

        public boolean canStart() {
            if (FerrobearEntity.this.isBaby()) {
                return false;
            } else {
                if (super.canStart()) {
                    List<FerrobearEntity> list_1 = FerrobearEntity.this.world.getEntities(FerrobearEntity.class, FerrobearEntity.this.getBoundingBox().expand(8.0D, 4.0D, 8.0D));
                    Iterator var2 = list_1.iterator();

                    while(var2.hasNext()) {
                        FerrobearEntity polarBearEntity_1 = (FerrobearEntity)var2.next();
                        if (polarBearEntity_1.isBaby()) {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        protected double getFollowRange() {
            return super.getFollowRange() * 0.5D;
        }
    }

    class PolarBearRevengeGoal extends RevengeGoal {
        public PolarBearRevengeGoal() {
            super(FerrobearEntity.this);
        }

        public void start() {
            super.start();
            if (FerrobearEntity.this.isBaby()) {
                this.callSameTypeForRevenge();
                this.stop();
            }

        }

        protected void setMobEntityTarget(MobEntity mobEntity_1, LivingEntity livingEntity_1) {
            if (mobEntity_1 instanceof FerrobearEntity && !mobEntity_1.isBaby()) {
                super.setMobEntityTarget(mobEntity_1, livingEntity_1);
            }

        }
    }

    static class PolarBearEntityData implements EntityData {
        private PolarBearEntityData() {
        }

        // $FF: synthetic method
        PolarBearEntityData(Object polarBearEntity$1_1) {
            this();
        }
    }
}
