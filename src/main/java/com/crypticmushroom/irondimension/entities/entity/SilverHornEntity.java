package com.crypticmushroom.irondimension.entities.entity;

import com.crypticmushroom.irondimension.entities.goals.EatIronGrassGoal;
import com.crypticmushroom.irondimension.registry.EntitiesID;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.DyeItem;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeType;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IWorld;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.World;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;

public class SilverHornEntity extends IronDimensionEntity {
    private int field_6865;
    private EatIronGrassGoal eatGrassGoal;

    public SilverHornEntity(EntityType<? extends IronDimensionEntity> entityType_1, World world_1) {
        super(entityType_1, world_1);
    }

    protected void initGoals() {
        this.eatGrassGoal = new EatIronGrassGoal(this);
        this.goalSelector.add(0, new SwimGoal(this));
        this.goalSelector.add(1, new EscapeDangerGoal(this, 1.25D));
        this.goalSelector.add(2, new AnimalMateGoal(this, 1.0D));
        this.goalSelector.add(3, new TemptGoal(this, 1.1D, Ingredient.ofItems(Items.WHEAT), false));
        this.goalSelector.add(4, new FollowParentGoal(this, 1.1D));
        this.goalSelector.add(5, this.eatGrassGoal);
        this.goalSelector.add(6, new WanderAroundFarGoal(this, 1.0D));
        this.goalSelector.add(7, new LookAtEntityGoal(this, PlayerEntity.class, 6.0F));
        this.goalSelector.add(8, new LookAroundGoal(this));
    }

    protected void mobTick() {
        this.field_6865 = this.eatGrassGoal.getTimer();
        super.mobTick();
    }

    public void tickMovement() {
        if (this.world.isClient) {
            this.field_6865 = Math.max(0, this.field_6865 - 1);
        }

        super.tickMovement();
    }

    protected void initAttributes() {
        super.initAttributes();
        this.getAttributeInstance(EntityAttributes.MAX_HEALTH).setBaseValue(16.0D);
        this.getAttributeInstance(EntityAttributes.MOVEMENT_SPEED).setBaseValue(0.23000000417232513D);
    }

    @Environment(EnvType.CLIENT)
    public void handleStatus(byte byte_1) {
        if (byte_1 == 10) {
            this.field_6865 = 40;
        } else {
            super.handleStatus(byte_1);
        }

    }

    @Environment(EnvType.CLIENT)
    public float method_6628(float float_1) {
        if (this.field_6865 <= 0) {
            return 0.0F;
        } else if (this.field_6865 >= 4 && this.field_6865 <= 36) {
            return 1.0F;
        } else {
            return this.field_6865 < 4 ? ((float)this.field_6865 - float_1) / 4.0F : -((float)(this.field_6865 - 40) - float_1) / 4.0F;
        }
    }

    @Environment(EnvType.CLIENT)
    public float method_6641(float float_1) {
        if (this.field_6865 > 4 && this.field_6865 <= 36) {
            float float_2 = ((float)(this.field_6865 - 4) - float_1) / 32.0F;
            return 0.62831855F + 0.21991149F * MathHelper.sin(float_2 * 28.7F);
        } else {
            return this.field_6865 > 0 ? 0.62831855F : this.pitch * 0.017453292F;
        }
    }

    protected SoundEvent getAmbientSound() {
        return SoundEvents.ENTITY_SHEEP_AMBIENT;
    }

    protected SoundEvent getHurtSound(DamageSource damageSource_1) {
        return SoundEvents.ENTITY_SHEEP_HURT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_SHEEP_DEATH;
    }

    protected void playStepSound(BlockPos blockPos_1, BlockState blockState_1) {
        this.playSound(SoundEvents.ENTITY_SHEEP_STEP, 0.15F, 1.0F);
    }

    public void onEatingGrass() {
        if (this.isBaby()) {
            this.growUp(60);
        }

    }

    protected float getActiveEyeHeight(EntityPose entityPose_1, EntityDimensions entityDimensions_1) {
        return 0.95F * entityDimensions_1.height;
    }

    // $FF: synthetic method
    public PassiveEntity createChild(PassiveEntity var1) {
        return (PassiveEntity) EntitiesID.SILVERHORN.create(this.world);
    }
}
