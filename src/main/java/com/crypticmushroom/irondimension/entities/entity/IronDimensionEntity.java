package com.crypticmushroom.irondimension.entities.entity;

import com.crypticmushroom.irondimension.IronDimension;
import com.crypticmushroom.irondimension.registry.BlocksID;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnType;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.FishEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.ViewableWorld;
import net.minecraft.world.World;

import java.util.Random;

public abstract class IronDimensionEntity extends AnimalEntity {
    public IronDimensionEntity(EntityType<? extends IronDimensionEntity> entityType_1, World world_1) {
        super(entityType_1, world_1);
    }

    @Override
    public float getPathfindingFavor(BlockPos blockPos_1, ViewableWorld viewableWorld_1) {
        return viewableWorld_1.getBlockState(blockPos_1.down()).getBlock() == BlocksID.iron_grass ? 10.0F : viewableWorld_1.getBrightness(blockPos_1) - 0.5F;
    }

    public static boolean canSpawnIron(EntityType<? extends MobEntity> entityType_1, IWorld iWorld_1, SpawnType spawnType_1, BlockPos blockPos_1, Random random_1) {
        return iWorld_1.getBlockState(blockPos_1.down()).getBlock() == BlocksID.iron_grass && iWorld_1.getLightLevel(blockPos_1, 0) > 8;
    }
}
