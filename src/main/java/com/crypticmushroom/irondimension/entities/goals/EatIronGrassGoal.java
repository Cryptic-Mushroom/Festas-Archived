package com.crypticmushroom.irondimension.entities.goals;

import com.crypticmushroom.irondimension.registry.BlocksID;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.predicate.block.BlockStatePredicate;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;

import java.util.EnumSet;
import java.util.function.Predicate;

public class EatIronGrassGoal extends Goal {
    private static final Predicate<BlockState> GRASS_PREDICATE;
    private final MobEntity mob;
    private final World world;
    private int timer;

    public EatIronGrassGoal(MobEntity mobEntity_1) {
        this.mob = mobEntity_1;
        this.world = mobEntity_1.world;
        this.setControls(EnumSet.of(Goal.Control.MOVE, Goal.Control.LOOK, Goal.Control.JUMP));
    }

    public boolean canStart() {
        if (this.mob.getRand().nextInt(this.mob.isBaby() ? 50 : 1000) != 0) {
            return false;
        } else {
            BlockPos blockPos_1 = new BlockPos(this.mob);
            if (GRASS_PREDICATE.test(this.world.getBlockState(blockPos_1))) {
                return true;
            } else {
                return this.world.getBlockState(blockPos_1.down()).getBlock() == BlocksID.iron_grass;
            }
        }
    }

    public void start() {
        this.timer = 40;
        this.world.sendEntityStatus(this.mob, (byte)10);
        this.mob.getNavigation().stop();
    }

    public void stop() {
        this.timer = 0;
    }

    public boolean shouldContinue() {
        return this.timer > 0;
    }

    public int getTimer() {
        return this.timer;
    }

    public void tick() {
        this.timer = Math.max(0, this.timer - 1);
        if (this.timer == 4) {
            BlockPos blockPos_1 = new BlockPos(this.mob);
            if (GRASS_PREDICATE.test(this.world.getBlockState(blockPos_1))) {
                if (this.world.getGameRules().getBoolean(GameRules.MOB_GRIEFING)) {
                    this.world.breakBlock(blockPos_1, false);
                }

                this.mob.onEatingGrass();
            } else {
                BlockPos blockPos_2 = blockPos_1.down();
                if (this.world.getBlockState(blockPos_2).getBlock() == BlocksID.iron_grass) {
                    if (this.world.getGameRules().getBoolean(GameRules.MOB_GRIEFING)) {
                        this.world.playLevelEvent(2001, blockPos_2, Block.getRawIdFromState(BlocksID.iron_grass.getDefaultState()));
                        this.world.setBlockState(blockPos_2, BlocksID.iron_dirt.getDefaultState(), 2);
                    }

                    this.mob.onEatingGrass();
                }
            }

        }
    }

    static {
        GRASS_PREDICATE = BlockStatePredicate.forBlock(BlocksID.acidrose_bush);
    }
}
