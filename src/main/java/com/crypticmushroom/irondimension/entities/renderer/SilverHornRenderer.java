package com.crypticmushroom.irondimension.entities.renderer;

import com.crypticmushroom.irondimension.entities.entity.SilverHornEntity;
import com.crypticmushroom.irondimension.entities.models.SilverHornSheepModel;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.render.entity.model.SheepEntityModel;
import net.minecraft.util.Identifier;

public class SilverHornRenderer extends MobEntityRenderer<SilverHornEntity, SilverHornSheepModel<SilverHornEntity>> {
    private static final Identifier SKIN = new Identifier("textures/entity/sheep/sheep.png");

    public SilverHornRenderer(EntityRenderDispatcher entityRenderDispatcher_1) {
        super(entityRenderDispatcher_1, new SilverHornSheepModel(), 0.7F);
    }

    @Override
    protected Identifier getTexture(SilverHornEntity var1) {
        return SKIN;
    }
}
