package com.crypticmushroom.irondimension.entities.renderer;

import com.crypticmushroom.irondimension.entities.entity.FerrobearEntity;
import com.crypticmushroom.irondimension.entities.models.FerrobearPolarbearModel;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.render.entity.model.PolarBearEntityModel;
import net.minecraft.util.Identifier;

public class FerrobearRenderer extends MobEntityRenderer<FerrobearEntity, FerrobearPolarbearModel<FerrobearEntity>> {

    private static final Identifier SKIN = new Identifier("textures/entity/bear/polarbear.png");

    public FerrobearRenderer(EntityRenderDispatcher entityRenderDispatcher_1) {
        super(entityRenderDispatcher_1, new FerrobearPolarbearModel(), 0.7F);
    }

    @Override
    protected Identifier getTexture(FerrobearEntity var1) {
        return SKIN;
    }
}
