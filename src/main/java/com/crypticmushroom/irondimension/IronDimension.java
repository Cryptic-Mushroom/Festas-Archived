package com.crypticmushroom.irondimension;

import com.crypticmushroom.irondimension.blocks.BlockIronPortal;
import com.crypticmushroom.irondimension.registry.BlocksID;
import com.crypticmushroom.irondimension.registry.EntitiesID;
import com.crypticmushroom.irondimension.registry.FluidsID;
import com.crypticmushroom.irondimension.registry.ItemsID;
import com.crypticmushroom.irondimension.world.WorldIronDimension;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.Identifier;

public class IronDimension implements ModInitializer {
    public static EnumProperty<BlockIronPortal.State> TYPE;

    public static final String MODID = "irondimension";

    public static final ItemGroup IDL_ITEM_GROUP = FabricItemGroupBuilder.build(new Identifier(MODID, "itemgroup"), () -> new ItemStack(Items.IRON_INGOT));

    @Override
    public void onInitialize() {
        TYPE = EnumProperty.of("state", BlockIronPortal.State.class);
        BlocksID.init();
        ItemsID.init();
        FluidsID.init();
        EntitiesID.init();
        WorldIronDimension.registerWorld();
    }
}
