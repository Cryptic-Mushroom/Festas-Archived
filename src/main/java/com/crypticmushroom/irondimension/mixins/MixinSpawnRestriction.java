package com.crypticmushroom.irondimension.mixins;

import com.crypticmushroom.irondimension.entities.entity.IronDimensionEntity;
import com.crypticmushroom.irondimension.registry.EntitiesID;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnRestriction;
import net.minecraft.entity.SpawnRestrictionEntry;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.Heightmap;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Map;

@Mixin(SpawnRestriction.class)
public class MixinSpawnRestriction {

    @Shadow
    private static <T extends MobEntity> void setRestrictions(EntityType<T> entityType_1, SpawnRestriction.Location spawnRestriction$Location_1, Heightmap.Type heightmap$Type_1, SpawnRestriction.class_4306<T> spawnRestriction$class_4306_1) {
        SpawnRestrictionEntry spawnRestriction$Entry_1 = (SpawnRestrictionEntry) mapping.put(entityType_1, new SpawnRestrictionEntry(heightmap$Type_1, spawnRestriction$Location_1, spawnRestriction$class_4306_1));
        if (spawnRestriction$Entry_1 != null) {
            throw new IllegalStateException("Duplicate registration for type " + Registry.ENTITY_TYPE.getId(entityType_1));
        }
    }

    @Shadow @Final private static Map<EntityType<?>, SpawnRestrictionEntry> mapping;

    static {
        setRestrictions(EntitiesID.IRONBISON, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, IronDimensionEntity::canSpawnIron);
        setRestrictions(EntitiesID.SILVERHORN, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, IronDimensionEntity::canSpawnIron);
        setRestrictions(EntitiesID.FERROBEAR, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, IronDimensionEntity::canSpawnIron);
    }
}
