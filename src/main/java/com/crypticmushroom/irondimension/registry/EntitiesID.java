package com.crypticmushroom.irondimension.registry;

import com.crypticmushroom.irondimension.IronDimension;
import com.crypticmushroom.irondimension.entities.entity.FerrobearEntity;
import com.crypticmushroom.irondimension.entities.entity.IronBisonEntity;

import com.crypticmushroom.irondimension.entities.entity.SilverHornEntity;
import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCategory;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.Item;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * EntitiesID
 */
public class EntitiesID {

    public static EntityType<MobEntity> IRONBISON;
    public static EntityType<MobEntity> SILVERHORN;
    public static EntityType<MobEntity> FERROBEAR;

    public static void init() {
        IRONBISON = (EntityType)Registry.register(Registry.ENTITY_TYPE,
                new Identifier(IronDimension.MODID, "iron_bison"), FabricEntityTypeBuilder.create(EntityCategory.CREATURE,
                        IronBisonEntity::new).size(new EntityDimensions(0.9F, 1.4F, true)).build());
        SILVERHORN = (EntityType)Registry.register(Registry.ENTITY_TYPE,
                new Identifier(IronDimension.MODID, "silverhorn"), FabricEntityTypeBuilder.create(EntityCategory.CREATURE,
                        SilverHornEntity::new).size(new EntityDimensions(0.9F, 1.3F, true)).build());
        FERROBEAR = (EntityType)Registry.register(Registry.ENTITY_TYPE,
                new Identifier(IronDimension.MODID, "ferrobear"), FabricEntityTypeBuilder.create(EntityCategory.CREATURE,
                        FerrobearEntity::new).size(new EntityDimensions(1.4F, 1.4F, true)).build());
        ItemsID.iron_bison_spawn_egg = register("iron_bison_spawn_egg",
                new SpawnEggItem(EntitiesID.IRONBISON, 4470310, 10592673, new Item.Settings().group(IronDimension.IDL_ITEM_GROUP)));
        ItemsID.silverhorn_spawn_egg = register("silverhorn_spawn_egg",
                new SpawnEggItem(EntitiesID.SILVERHORN, 4470310, 10592673, new Item.Settings().group(IronDimension.IDL_ITEM_GROUP)));
        ItemsID.ferrobear_spawn_egg = register("ferrobear_spawn_egg",
                new SpawnEggItem(EntitiesID.FERROBEAR, 4470310, 10592673, new Item.Settings().group(IronDimension.IDL_ITEM_GROUP)));
    }

    private static Item register(String name, Item item) {
        Registry.register(Registry.ITEM, IronDimension.MODID + ":" + name, item);
        return item;
    }
}