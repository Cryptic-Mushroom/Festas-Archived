package com.crypticmushroom.irondimension.items;

import com.crypticmushroom.irondimension.IronDimension;
import net.minecraft.item.Item;

public class ItemIDLGeneric extends Item {
    public ItemIDLGeneric() {
        super(new Item.Settings().group(IronDimension.IDL_ITEM_GROUP));
    }
}
