package com.crypticmushroom.irondimension.blocks;

import com.crypticmushroom.irondimension.registry.ItemsID;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SignBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.world.loot.context.LootContext;

import java.util.ArrayList;
import java.util.List;

public class IronSignBlock extends SignBlock {
    public IronSignBlock(Block block) {
        super(FabricBlockSettings.of(block.getMaterial(block.getDefaultState()), block.getMaterial(block.getDefaultState()).getColor()).noCollision().hardness(1.0F).sounds(BlockSoundGroup.WOOD).build());
    }
}
