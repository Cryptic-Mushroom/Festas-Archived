package com.crypticmushroom.irondimension.blocks;

import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tag.FabricItemTags;
import net.minecraft.block.BlockState;
import net.minecraft.block.LogBlock;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.world.loot.context.LootContext;

import java.util.ArrayList;
import java.util.List;

public class BlockIronLog extends LogBlock {
    public BlockIronLog(MaterialColor materialColor_1) {
        super(materialColor_1, FabricBlockSettings
                .of(Material.WOOD, materialColor_1)
                .breakByTool(FabricItemTags.AXES, 2)
                .sounds(BlockSoundGroup.WOOD)
                .hardness(2.0F)
                .resistance(2.0F)
                .build());
    }

    @Override
    public List<ItemStack> getDroppedStacks(BlockState blockState_1, LootContext.Builder lootContext$Builder_1) {
        super.getDroppedStacks(blockState_1, lootContext$Builder_1);
        List<ItemStack> list = new ArrayList<>();
        list.add(new ItemStack(this));
        return list;
    }
}
