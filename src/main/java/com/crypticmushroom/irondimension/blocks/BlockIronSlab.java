package com.crypticmushroom.irondimension.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SlabBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.loot.context.LootContext;

import java.util.ArrayList;
import java.util.List;

public class BlockIronSlab extends SlabBlock {
    public BlockIronSlab(Block block$Settings_1) {
        super(Block.Settings.copy(block$Settings_1));
    }

    @Override
    public List<ItemStack> getDroppedStacks(BlockState blockState_1, LootContext.Builder lootContext$Builder_1) {
        super.getDroppedStacks(blockState_1, lootContext$Builder_1);
        List<ItemStack> list = new ArrayList<>();
        list.add(new ItemStack(this));
        return list;
    }
}
