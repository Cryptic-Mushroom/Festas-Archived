package com.crypticmushroom.irondimension.blocks;

import net.minecraft.block.FluidBlock;
import net.minecraft.fluid.BaseFluid;

public class BlockFluid extends FluidBlock {
    public BlockFluid(BaseFluid baseFluid_1, Settings block$Settings_1) {
        super(baseFluid_1, block$Settings_1);
    }
}
