package net.minecraft.entity;

import net.minecraft.world.Heightmap;

public class SpawnRestrictionEntry extends SpawnRestriction.Entry {
    public SpawnRestrictionEntry(Heightmap.Type heightmap$Type_1, SpawnRestriction.Location spawnRestriction$Location_1, SpawnRestriction.class_4306 class_4306) {
        super(heightmap$Type_1, spawnRestriction$Location_1, class_4306);
    }
}
