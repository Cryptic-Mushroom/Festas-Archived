# DEVELOPMENT PAUSED  
Development for Festas is currently inactive. It is not decided whether or not the project will continue and, as such, is paused for now.

# Iron Dimension Legacy  
The revised port of the original Iron Dimension mod.  
This mod, originally maintained by bgwoo, adds a new dimension to the mod, the Iron Dimension. Everything here is made of Iron, including the plant life and animals. Now maintained by Cryptic Mushroom, the plan is to expand from where the mod left off and make improvements from there. This include enchanced features and additions to the mod that expand on the dimension, or just as some extra quality.

## Where can you find us?  
If you want to discuss the mod, we have a [Discord server](https://discord.gg/Rdc86yA)!  
We have other mods we like to work on, too! You can view them by visiting our [GitHub page](https://github.com/Cryptic-Mushroom) and [Cipher_Zero_X's CurseForge page](https://www.curseforge.com/members/cipher_zero_x/projects).  
If you would like to support our work, consider donating to our [Patreon](https://www.patreon.com/crypticmushroom).

### Credits (will be updated if development resumes)  
- **Project Leader**: [Cipher_Zero_X](https://github.com/cipherzerox)
- **Everyone else**: [GitHub Repository Contributors](https://github.com/Cryptic-Mushroom/Planet-Bound/graphs/contributors)